$(document).ready(function () {
    $(".review-slider").owlCarousel({
        loop: true,
        // nav: true,
        dots: true,
        autoWidth: true,
        margin: 10,
        autoplayTimeout: 20000,
    });
});
