function addOrderHistory(dis, type = "order") {
    let valid = true;
    if (type === "payment") {
        let paymentMode = $(dis).find("#payment_mode option:selected").val(); // get payment mode
        let paymentMethod = $(dis).find("#order-payment-method").val(); // get payment_method
        let paymentType = $(dis).find("#order-payment-type").val(); // get payment_type
        let remainingAmount = amountSanitizer(
            $(dis).find("#order-remaining-amount").val()
        ); // get remaining_amount
        let totalAmount = amountSanitizer(
            $(dis).find("#order-total-amount").val()
        ); // get total_amount

        if (paymentMode === "cash") {
            if (paymentMethod === "COD") {
                let notesGrandTotal = calculateNotesTotal(); // get notes total
                if (notesGrandTotal !== totalAmount) {
                    valid = false;
                    alert(
                        "The calculated bills grand total is not equal to order grand total!"
                    );
                }
            }
            if (paymentType === "partial") {
                let notesGrandTotal = calculateNotesTotal(); // get notes total
                if (notesGrandTotal !== remainingAmount) {
                    valid = false;
                    alert(
                        "The calculated bills grand total is not equal to order remaining amount!"
                    );
                }
            }
        }
    }
    return valid;
}

function calculateNotesTotal() {
    const notes = {
        hundred: 100,
        fifty: 50,
        twenty: 20,
        ten: 10,
        five: 5,
        two: 2,
        one: 1,
    };
    let notesGrandTotal = 0;
    for (const key in notes) {
        let noteCount =
            $("#" + key).val() !== undefined &&
            $("#" + key).val() !== NaN &&
            $("#" + key).val() !== null &&
            $("#" + key).val() !== ""
                ? parseInt($("#" + key).val())
                : 0;
        notesGrandTotal += parseInt(notes[key]) * noteCount;
    }
    return notesGrandTotal;
}

function amountSanitizer(eleWithAttr) {
    return eleWithAttr !== undefined &&
        eleWithAttr !== NaN &&
        eleWithAttr !== null &&
        eleWithAttr !== ""
        ? parseInt(eleWithAttr)
        : 0;
}

function loadUpdateOrderModal(dis, id, url, type = "order") {
    let parent = type === "order" ? $("#add-route") : $("#update-payment");
    $.ajax({
        type: "GET",
        url: url,
        dataType: "JSON",
        success: function (res) {
            if (res.status) {
                parent.find("#order-detail").html(res.data);
                parent.modal("show");
                parent.find("#order-id").val(id); // set order_id
                let paymentMethod = $(dis)
                    .closest("tr")
                    .attr("data-payment-method"); // get payment_method
                let paymentType = $(dis)
                    .closest("tr")
                    .attr("data-payment-type"); // get payment_type
                let remainingAmount = amountSanitizer(
                    $(dis).closest("tr").attr("data-remaining-amount")
                ); // get remaining_amount
                let totalAmount = amountSanitizer(
                    $(dis).closest("tr").attr("data-total-amount")
                ); // get total_amount
                let orderStatus = $(dis)
                    .closest("tr")
                    .attr("data-order-status"); // get order_status
                let orderStatusId = $(dis)
                    .closest("tr")
                    .attr("data-order-status-id"); // get order_status_id

                parent.find("#order-payment-method").val(paymentMethod);
                parent.find("#order-payment-type").val(paymentType);
                parent.find("#order-remaining-amount").val(remainingAmount);
                parent.find("#order-total-amount").val(totalAmount);
                if (parseInt(remainingAmount) === 0) {
                    $("#payment-received").val(true);
                }
                parent.find("#order-status-id").val(orderStatusId);
                parent.find("#order-status").val(orderStatus);

                /**
                 * preselect order status if matches in case of `Update Order`
                 */
                if (type === "order") {
                    let exists =
                        0 !=
                        $(
                            "#order_status_id option[value=" +
                                orderStatusId +
                                "]"
                        ).length;

                    if (exists) {
                        $(
                            "#order_status_id option[value=" +
                                orderStatusId +
                                "]"
                        ).prop("selected", true);
                    }

                    handleRemoveOrderVisibility($("#order_status_id"));
                }

                if (paymentMethod === "COD" || paymentType === "partial") {
                    parent.find("#payment-mode-section").removeClass("d-none");
                }
            }
        },
        error: function (err) {
            console.log(
                "🚀 ~ file: route.js ~ line 74 ~ loadUpdateOrderModal ~ err",
                err
            );
        },
    });
}

function loadOptimizationModal(optimization_url) {
    $(".custom-loader").removeClass("d-none");
    $.ajax({
        type: "POST",
        url: optimization_url,
        data: {
            _token: CSRF_TOKEN,
        },
        dataType: "JSON",
        success: function (res) {
            $(".custom-loader").addClass("d-none");
            if (res.status) {
                $("#optimization-res-div").html(res.data);
            }
        },
        error: function (err) {
            console.log(
                "🚀 ~ file: index.blade.php ~ line 243 ~ routeOptimization ~ err",
                err
            );
        },
    });
}

function updateRoutes(url) {
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: CSRF_TOKEN,
        },
        dataType: "JSON",
        success: function (res) {
            if (res.status) {
                $("#optimize-route-modal").modal("hide");
                toastr.success(
                    "Successfully optimized route delivery route.",
                    ""
                );
                location.reload();
            }
        },
        error: function (err) {
            console.log(
                "🚀 ~ file: index.blade.php ~ line 243 ~ routeOptimization ~ err",
                err
            );
        },
    });
}

function getLatLng(dis, url) {
    $(".custom-loader").removeClass("d-none");
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: CSRF_TOKEN,
        },
        dataType: "JSON",
        success: function (res) {
            $(".custom-loader").addClass("d-none");
            if (res.status) {
                toastr.success(res.data, "");
                $(dis).remove();
            } else {
                toastr.error(res.data, "");
            }
            if ($(".get-lat-lng").length === 0) {
                location.reload();
            }
        },
        error: function (err) {
            console.log(
                "🚀 ~ file: route.js ~ line 145 ~ getLatLng ~ err",
                err
            );
        },
    });
}

function copyToClipboard(dis) {
    initCopyToClipBoard(
        "#copy",
        true,
        document.getElementById("unoptimized-routes"),
        $("#addresses").val()
    );
}

function removeAddress(dis, url) {
    $(".custom-loader").removeClass("d-none");
    $.ajax({
        type: "GET",
        url: url,
        dataType: "JSON",
        success: function (res) {
            $(".custom-loader").addClass("d-none");
            if (res.status) {
                toastr.success(res.data, "");
                $(dis).closest("tr").remove();
            } else {
                toastr.error(res.data, "");
            }
        },
        error: function (err) {
            console.log(
                "🚀 ~ file: route.js ~ line 145 ~ getLatLng ~ err",
                err
            );
        },
    });
}

function hideShowBillSection() {
    let value = $("#payment_mode option:selected").val();

    if (value === "cash") {
        $(".bills-section").removeClass("d-none");
    } else {
        $(".bills-section").addClass("d-none");
    }
}

function copySingleAddress(dis) {
    initCopyToClipBoard(
        "#copy-address",
        false,
        "",
        $(dis).closest("tr").find(".address").val()
    );
}

function generateLoadingSheet(dis, route_id, url) {
    $(".custom-loader").removeClass("d-none");
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: CSRF_TOKEN,
            route_id: route_id,
        },
        dataType: "JSON",
        success: function (res) {
            if (res.status) {
                toastr.success("Successfully generated loading sheet");
                $(".custom-loader").addClass("d-none");
                window.location =
                    BASE_URL + "loading-sheets/detail/" + res.loading_sheet_id;
            }
        },
        error: function (err) {
            console.log(
                "🚀 ~ file: route.js ~ line 235 ~ generateLoadingSheet ~ err",
                err
            );
        },
    });
}

function loadOrdersModal(dis, url) {
    $(".custom-loader").removeClass("d-none");
    $.ajax({
        type: "GET",
        url: url,
        dataType: "JSON",
        success: function (res) {
            $("#order-modal-section").html(res.data);
            $(".custom-loader").addClass("d-none");
            initDataTable(
                $(document).find("#generic-datatable"),
                setConfigOptions()
            );
            $("#orders-modal").modal("show");
        },
        error: function (err) {
            console.log(
                "🚀 ~ file: route.js ~ line 272 ~ loadOrdersModal ~ err",
                err
            );
        },
    });
}

function loadRouteSummary(dis, url) {
    $(".custom-loader").removeClass("d-none");
    $.ajax({
        type: "GET",
        url: url,
        dataType: "JSON",
        success: function (res) {
            if (res.status) {
                $(".custom-loader").addClass("d-none");
                $("#summary").html(res.data);
                $("#route-summary-modal").modal("show");
            }
        },
        error: function (err) {
            console.log(
                "🚀 ~ file: route.js ~ line 321 ~ loadRouteSummary ~ err",
                err
            );
        },
    });
}

function handleRemoveOrderVisibility(dis) {
    if ($(dis).val() == "17") {
        $("#remove-order-div").removeClass("d-none");
    } else {
        $("#remove-order-div").addClass("d-none");
    }
}
