<table border="" cellpadding="8" style="border-spacing: 0px; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;">
   @php
   $total_options = count($options);
   $a = 0;
   @endphp

   @if ($loading_sheet)

   <thead>
      <tr>
         <th colspan="{{2 + $total_options}}" rowspan="2" style="text-align: center;"><strong>{{date('F jS, Y', strtotime($loading_sheet->created_at))}}</strong></th>
      </tr>
      <tr>
         <th colspan="{{2 + $total_options}}"></th>
      </tr>
      <tr>
         <th><strong>Product</strong></th>
         @foreach ($options as $val)
         <th><strong>{{$val->eng_description->name}}</strong></th>
         @endforeach
         <th><strong>Mattress</strong></th>
      </tr>
   </thead>

   <tbody>
      @if(count($items) > 0)
      @foreach($items as $loading_sheet_item)
      @php
      $a++;
      @endphp
      <tr>
         <td>
            {{$loading_sheet_item['unformatted_name']}}
         </td>
         @foreach ($options as $val)
         <td>
            @if(count($loading_sheet_item['loading_sheet_item_options']) > 0)
            @foreach($loading_sheet_item['loading_sheet_item_options'] as $option)
            @if ($val->eng_description->name == $option->name)
            {{$option->value}},&nbsp;
            @endif
            @endforeach
            @endif
         </td>
         @endforeach
         <td style="text-align: right;">{{$loading_sheet_item['quantity']}}</td>
      </tr>
      @endforeach
      @else
      <tr>
         <td colspan="{{2 + $total_options}}">No Data found...</td>
      </tr>
      @endif
   </tbody>

   @endif
</table>