<div id="kt_aside" class="aside aside-dark aside-hoverable" data-kt-drawer="true" data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true"
    data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_mobile_toggle">
    <!--begin::Brand-->
    <div class="aside-logo flex-column-auto" id="kt_aside_logo">
        <!--begin::Logo-->
        <a href="{{route('dashboard.index')}}">
            <img alt="Logo" src="{{asset('metronic/')}}/media/logos/logo-white.jpg" class="h-55px logo" />
        </a>
        <!--end::Logo-->
        <!--begin::Aside toggler-->
        <div id="kt_aside_toggle" class="btn btn-icon w-auto px-0 btn-active-color-primary aside-toggle" data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body"
            data-kt-toggle-name="aside-minimize">
            <!--begin::Svg Icon | path: icons/duotone/Navigation/Angle-double-left.svg-->
            <span class="svg-icon svg-icon-1 rotate-180">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <polygon points="0 0 24 0 24 24 0 24" />
                        <path
                            d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z"
                            fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999)" />
                        <path
                            d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z"
                            fill="#000000" fill-rule="nonzero" opacity="0.5" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999)" />
                    </g>
                </svg>
            </span>
            <!--end::Svg Icon-->
        </div>
        <!--end::Aside toggler-->
    </div>
    <!--end::Brand-->
    <!--begin::Aside menu-->
    <div class="aside-menu flex-column-fluid">
        <!--begin::Aside Menu-->
        <div class="hover-scroll-overlay-y my-2 py-5 py-lg-8" id="kt_aside_menu_wrapper" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto"
            data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer" data-kt-scroll-wrappers="#kt_aside_menu" data-kt-scroll-offset="0">
            <!--begin::Menu-->
            <div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500" id="#kt_aside_menu" data-kt-menu="true">
                <div class="menu-item">
                    <a class="menu-link {{(isset($active) && $active =='dashboard') ? 'active' : ''}}" href="{{route('dashboard.index')}}">
                        <span class="menu-icon">
                            <i class="bi bi-speedometer2 fs-3"></i>
                        </span>
                        <span class="menu-title">Dashboard</span>
                    </a>
                </div>

                @can('Browse-Catalog')
                <div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {{(isset($menu_1) && $menu_1 == 'catalog') ? 'here show' : ''}}">
                    <span class="menu-link">
                        <span class="menu-icon">
                            <i class="bi bi-tags fs-3"></i>
                        </span>
                        <span class="menu-title">Catalog</span>
                        <span class="menu-arrow"></span>
                    </span>
                    <div class="menu-sub menu-sub-accordion">
                        @can('Browse-Categories')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'categories') ? 'active' : ''}}" href="{{route('categories.index')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Category</span>
                            </a>
                        </div>
                        @endcan

                        @can('Browse-Products')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'products') ? 'active' : ''}}" href="{{route('products.index')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Product</span>
                            </a>
                        </div>
                        @endcan

                        @can('Browse-Manufacturers')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'manufacturers') ? 'active' : ''}}" href="{{route('manufacturers.index')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Manufacturer</span>
                            </a>
                        </div>
                        @endcan

                        @can('Browse-Attribute-Manager')
                        <div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {{(isset($sub_menu) && $sub_menu == 'attribute_manager') ? 'here show' : ''}}">
                            <span class="menu-link">
                                <span class="menu-icon">
                                    <i class="icon-md la la-angle-double-right"></i>
                                </span>
                                <span class="menu-title">Attribute Manager</span>
                                <span class="menu-arrow"></span>
                            </span>
                            <div class="menu-sub menu-sub-accordion">
                                @can('Browse-Attribute-Groups')
                                <div class="menu-item">
                                    <a class="menu-link {{(isset($active) && $active == 'attribute-groups') ? 'active' : ''}}" href="{{route('attribute-groups.index')}}">
                                        <span class="menu-bullet">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">Attribute Groups</span>
                                    </a>
                                </div>
                                @endcan

                                @can('Browse-Attributes')
                                <div class="menu-item">
                                    <a class="menu-link {{(isset($active) && $active == 'attributes') ? 'active' : ''}}" href="{{route('attributes.index')}}">
                                        <span class="menu-bullet">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">Attributes</span>
                                    </a>
                                </div>
                                @endcan

                            </div>
                        </div>
                        @endcan

                        @can('Browse-Options')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'options') ? 'active' : ''}}" href="{{route('options.index')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Option</span>
                            </a>
                        </div>
                        @endcan
                    </div>
                </div>
                @endcan

                @can('Browse-Sales')
                <div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {{(isset($menu_1) && $menu_1 == 'sales') ? 'here show' : ''}}">
                    <span class="menu-link">
                        <span class="menu-icon">
                            <i class="bi bi-cart-check fs-3"></i>
                        </span>
                        <span class="menu-title">Sales</span>
                        <span class="menu-arrow"></span>
                    </span>
                    <div class="menu-sub menu-sub-accordion">
                        @can('Browse-Orders')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'orders') ? 'active' : ''}}" href="{{route('orders.index')}}" data-bs-toggle="tooltip" data-bs-trigger="hover"
                                data-bs-dismiss="click" data-bs-placement="right" data-bs-original-title="" title="">
                                <span class="menu-icon">
                                    <i class="icon-md la la-angle-double-right"></i>
                                </span>
                                <span class="menu-title">Orders</span>
                            </a>
                        </div>
                        @endcan
                        @can('Add-Orders')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'orders-create') ? 'active' : ''}}" href="{{route('orders.create')}}" data-bs-toggle="tooltip" data-bs-trigger="hover"
                                data-bs-dismiss="click" data-bs-placement="right" data-bs-original-title="" title="">
                                <span class="menu-icon">
                                    <i class="icon-md la la-angle-double-right"></i>
                                </span>
                                <span class="menu-title">Create Order</span>
                            </a>
                        </div>
                        @endcan

                        @can('Browse-Routes')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'routes') ? 'active' : ''}}" href="{{route('routes.index')}}" data-bs-toggle="tooltip" data-bs-trigger="hover"
                                data-bs-dismiss="click" data-bs-placement="right" data-bs-original-title="" title="">
                                <span class="menu-icon">
                                    <i class="icon-md la la-angle-double-right"></i>
                                </span>
                                <span class="menu-title">Routes</span>
                            </a>
                        </div>
                        @endcan

                        @can('Browse-Loading-Sheets')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'loading-sheets') ? 'active' : ''}}" href="{{route('loading-sheets.index')}}" data-bs-toggle="tooltip"
                                data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right" data-bs-original-title="" title="">
                                <span class="menu-icon">
                                    <i class="icon-md la la-angle-double-right"></i>
                                </span>
                                <span class="menu-title">Loading Sheets</span>
                            </a>
                        </div>
                        @endcan
                    </div>
                </div>
                @endcan

                @can('Browse-Customer-Manager')
                <div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {{(isset($menu_1) && $menu_1 == 'customer-manager') ? 'here show' : ''}}">
                    <span class="menu-link">
                        <span class="menu-icon">
                            <i class="bi bi-person fs-3"></i>
                        </span>
                        <span class="menu-title">Customer Manager</span>
                        <span class="menu-arrow"></span>
                    </span>
                    <div class="menu-sub menu-sub-accordion">
                        @can('Browse-Customer-Groups')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'customer-groups') ? 'active' : ''}}" href="{{route('customer-groups.index')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Customer Groups</span>
                            </a>
                        </div>
                        @endcan

                        @can('Browse-Customers')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'customers') ? 'active' : ''}}" href="{{route('customers.index')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Customers</span>
                            </a>
                        </div>
                        @endcan

                        @can('Browse-Subscribers')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'subscribers') ? 'active' : ''}}" href="{{route('subscribers.index')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Subscribers</span>
                            </a>
                        </div>
                        @endcan

                        @can('Browse-Reviews')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'reviews') ? 'active' : ''}}" href="{{route('reviews.index')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Reviews</span>
                            </a>
                        </div>
                        @endcan
                    </div>
                </div>
                @endcan

                @can('Browse-System')
                <div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {{(isset($menu_1) && $menu_1 == 'system') ? 'here show' : ''}}">
                    <span class="menu-link">
                        <span class="menu-icon">
                            <i class="bi bi-gear fs-3"></i>
                        </span>
                        <span class="menu-title">System</span>
                        <span class="menu-arrow"></span>
                    </span>
                    <div class="menu-sub menu-sub-accordion">
                        @can('Browse-Settings')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'settings') ? 'active' : ''}}" href="{{route('settings.edit')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Settings</span>
                            </a>
                        </div>
                        @endcan
                        @can('Browse-Localization')
                        <div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {{(isset($sub_menu) && $sub_menu == 'localization') ? 'here show' : ''}}">
                            <span class="menu-link">
                                <span class="menu-icon">
                                    <i class="icon-md la la-angle-double-right"></i>
                                </span>
                                <span class="menu-title">Localization</span>
                                <span class="menu-arrow"></span>
                            </span>
                            <div class="menu-sub menu-sub-accordion">
                                @can('Browse-Stores')
                                <div class="menu-item">
                                    <a class="menu-link {{(isset($active) && $active == 'stores') ? 'active' : ''}}" href="{{route('stores.index')}}">
                                        <span class="menu-bullet">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">Stores</span></span>
                                    </a>
                                </div>
                                @endcan

                                @can('Browse-Pages')
                                <div class="menu-item">
                                    <a class="menu-link {{(isset($active) && $active == 'pages') ? 'active' : ''}}" href="{{route('pages.index')}}">
                                        <span class="menu-bullet">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">CMS Pages</span>
                                    </a>
                                </div>
                                @endcan

                                @can('Browse-Faqs')
                                <div class="menu-item">
                                    <a class="menu-link {{(isset($active) && $active == 'faqs') ? 'active' : ''}}" href="{{route('faqs.index')}}">
                                        <span class="menu-bullet">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">Faqs</span>
                                    </a>
                                </div>
                                @endcan

                                @can('Browse-Sliders')
                                <div class="menu-item">
                                    <a class="menu-link {{(isset($active) && $active == 'sliders') ? 'active' : ''}}" href="{{route('sliders.index')}}">
                                        <span class="menu-bullet">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">Sliders</span>
                                    </a>
                                </div>
                                @endcan

                                @can('Browse-Web-Notifications')
                                <div class="menu-item">
                                    <a class="menu-link {{(isset($active) && $active == 'web-notifications') ? 'active' : ''}}" href="{{route('web-notifications.index')}}">
                                        <span class="menu-bullet">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">Web Notifications</span>
                                    </a>
                                </div>
                                @endcan
                                {{-- @can('Browse-Store-Locations')
                                <div class="menu-item">
                                    <a class="menu-link {{(isset($active) && $active == 'store-locations') ? 'active' : ''}}" href="{{route('store-locations.index')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Store Location</span>
                                </a>
                            </div>
                            @endcan --}}
                            {{-- @can('Browse-Languages')
                            <div class="menu-item">
                                <a class="menu-link {{(isset($active) && $active == 'languages') ? 'active' : ''}}" href="{{route('languages.index')}}">
                            <span class="menu-bullet">
                                <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title">Languages</span>
                            </a>
                        </div>
                        @endcan --}}

                        @can('Browse-Currencies')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'currencies') ? 'active' : ''}}" href="{{route('currencies.index')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Currencies</span>
                            </a>
                        </div>
                        @endcan

                        @can('Browse-Stock-Statuses')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'stock-statuses') ? 'active' : ''}}" href="{{route('stock-statuses.index')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Stock Statuses</span>
                            </a>
                        </div>
                        @endcan

                        @can('Browse-Order-Statuses')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'order-statuses') ? 'active' : ''}}" href="{{route('order-statuses.index')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Order Statuses</span>
                            </a>
                        </div>
                        @endcan

                        @can('Browse-Countries')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'countries') ? 'active' : ''}}" href="{{route('countries.index')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Countries</span>
                            </a>
                        </div>
                        @endcan

                        @can('Browse-Zones')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'zones') ? 'active' : ''}}" href="{{route('zones.index')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">States</span>
                            </a>
                        </div>
                        @endcan

                        @can('Browse-Geozones')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'geozones') ? 'active' : ''}}" href="{{route('geozones.index')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Geo Zone</span>
                            </a>
                        </div>
                        @endcan

                        @can('Browse-Taxes')
                        <div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {{(isset($child_menu) && $child_menu == 'taxes') ? 'here show' : ''}}">
                            <span class="menu-link">
                                <span class="menu-icon">
                                    <i class="icon-md la la-angle-double-right"></i>
                                </span>
                                <span class="menu-title">Taxes</span>
                                <span class="menu-arrow"></span>
                            </span>
                            <div class="menu-sub menu-sub-accordion">
                                @can('Browse-Tax-Classes')
                                <div class="menu-item">
                                    <a class="menu-link {{(isset($active) && $active == 'tax-classes') ? 'active' : ''}}" href="{{route('tax-classes.index')}}">
                                        <span class="menu-bullet">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">Tax Classes</span>
                                    </a>
                                </div>
                                @endcan

                                @can('Browse-Tax-Rates')
                                <div class="menu-item">
                                    <a class="menu-link {{(isset($active) && $active == 'tax-rates') ? 'active' : ''}}" href="{{route('tax-rates.index')}}">
                                        <span class="menu-bullet">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">Tax Rates</span>
                                    </a>
                                </div>
                                @endcan
                            </div>
                        </div>
                        @endcan

                        @can('Browse-Length-Classes')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'length-classes') ? 'active' : ''}}" href="{{route('length-classes.index')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Length Classes</span>
                            </a>
                        </div>
                        @endcan

                        @can('Browse-Weight-Classes')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'weight-classes') ? 'active' : ''}}" href="{{route('weight-classes.index')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Weight Classes</span>
                            </a>
                        </div>
                        @endcan
                    </div>
                </div>
                @endcan

                @can('Browse-User-Management')
                <div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {{(isset($sub_menu) && $sub_menu == 'user_management') ? 'here show' : ''}}">
                    <span class="menu-link">
                        <span class="menu-icon">
                            <i class="icon-md la la-angle-double-right"></i>
                        </span>
                        <span class="menu-title">User Management</span>
                        <span class="menu-arrow"></span>
                    </span>
                    <div class="menu-sub menu-sub-accordion">
                        @can('Browse-Users')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'users') ? 'active' : ''}}" href="{{route('users.index')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Users</span>
                            </a>
                        </div>
                        @endcan

                        @can('Browse-Roles')
                        <div class="menu-item">
                            <a class="menu-link {{(isset($active) && $active == 'roles') ? 'active' : ''}}" href="{{route('roles.index')}}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">Roles</span>
                            </a>
                        </div>
                        @endcan

                    </div>
                </div>
                @endcan
            </div>
        </div>
        @endcan
    </div>
    <!--end::Menu-->
</div>
</div>
<!--end::Aside menu-->
</div>