<div class="row">
   <div class="col-md-6">
      <div class="table-responsive">
         <table class="table table-sm table-row-bordered table-striped table-row-gray-300 border gs-3 gy-3">
            <thead>
               <tr class="fw-bolder fs-6 text-gray-800">
                  <td>
                     <strong><i class="fas fa-user"></i> &nbsp;Customer Details</strong>
                  </td>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td>
                     <span title="Customer">
                        {{$order->first_name . ' ' . $order->last_name}}
                     </span>
                  </td>
               </tr>
               <tr>
                  <td>
                     <span title="Email">
                        {{$order->email}}
                     </span>
                  </td>
               </tr>
               <tr>
                  <td>
                     <span title="Telephone">
                        {{$order->telephone}}
                     </span>
                  </td>
               </tr>
            </tbody>
         </table>
      </div>
   </div>

   <div class="col-md-6">
      <div class="table-responsive">
         <table class="table table-sm table-row-bordered table-striped table-row-gray-300 border gs-3 gy-3">
            <thead>
               <tr class="fw-bolder fs-6 text-gray-800">
                  <td>
                     <strong><i class="fas fa-truck"></i> &nbsp;Shipping Details</strong>
                  </td>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td>{{$order->shipping_first_name .' '. $order->shipping_last_name}}</td>
               </tr>
               <tr>
                  <td>
                     @if (isset($order->shipping_postcode) && !is_null($order->shipping_postcode) && $order->shipping_postcode != "")
                     {{$order->shipping_city.", ". $order->shipping_postcode}}
                     @else
                     {{$order->shipping_city}}
                     @endif
                  </td>
               </tr>
               <tr>
                  <td>{{$order->shipping_zone .", ". $order->shipping_country}}</td>
               </tr>
               <tr>
                  <td>{{$order->shipping_address_1}}</td>
               </tr>
            </tbody>
         </table>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-md-12">
      <div class="table-responsive">
         <table class="table table-sm table-row-bordered table-striped table-row-gray-300 border gs-3 gy-3">
            <thead>
               <tr class="fw-bolder fs-6 text-gray-800">
                  <th style="width: 40%;">Product</th>
                  <th style="width: 20%;">Model</th>
                  <th class="text-end" style="width: 10%;">Qty</th>
                  <th class="text-end" style="width: 15%;">Unit Price</th>
                  <th class="text-end" style="width: 15%;">Total</th>
               </tr>
            </thead>
            <tbody>
               @foreach ($order->order_products as $product)
               <tr>
                  <td style="width: 40%;">
                     {{$product->name}}
                     @if(count($product->order_options) > 0)
                     @foreach($product->order_options as $option)
                     <br>
                     - {{$option->name}}: {{$option->value}}
                     @endforeach
                     @endif
                  </td>
                  <td style="width: 20%;">{{$product->product->model}}</td>
                  <td class="text-end" style="width: 10%;">{{$product->quantity}}</td>
                  <td class="text-end" style="width: 15%;">
                     @if (isset($order->currency->symbol_left) && !is_null($order->currency->symbol_left) && $order->currency->symbol_left !=
                     ""){{$order->currency->symbol_left}}@else $@endif{{setDefaultPriceFormat($product->price)}}
                  </td>
                  <td class="text-end" style="width: 15%;">
                     @if (isset($order->currency->symbol_left) && !is_null($order->currency->symbol_left) && $order->currency->symbol_left !=
                     ""){{$order->currency->symbol_left}}@else $@endif{{setDefaultPriceFormat($product->total)}}
                  </td>
               </tr>
               @endforeach
               @foreach ($order->order_totals as $total)
               <tr>
                  @if ($total->code == "shipping")
                  <td colspan="4" class="fw-bolder fs-6 text-gray-800 text-end">Shipping ({{$total->title}})</td>
                  <td class="fw-bolder fs-6 text-gray-800 text-end">@if (isset($order->currency->symbol_left) && !is_null($order->currency->symbol_left) &&
                     $order->currency->symbol_left
                     !=
                     ""){{$order->currency->symbol_left}}@else $@endif{{setDefaultPriceFormat($total->value)}}</td>
                  @elseif ($total->code == "payment_method")
                  <td colspan="4" class="fw-bolder fs-6 text-gray-800 text-end">Payment Method</td>
                  <td class="fw-bolder fs-6 text-gray-800 text-end">{{$total->title}}</td>
                  @elseif ($total->code == "payment_type")
                  <td colspan="4" class="fw-bolder fs-6 text-gray-800 text-end">Payment Type</td>
                  <td class="fw-bolder fs-6 text-gray-800 text-end">{{$total->title}}</td>
                  @elseif ($total->code == "payment_mode")
                  <td colspan="4" class="fw-bolder fs-6 text-gray-800 text-end">Payment Mode</td>
                  <td class="fw-bolder fs-6 text-gray-800 text-end">{{$total->title}}</td>
                  @elseif ($total->code == "discount")
                  <td colspan="4" class="fw-bolder fs-6 text-gray-800 text-end">Discount ({{$total->title}})</td>
                  <td class="fw-bolder fs-6 text-gray-800 text-end">@if (isset($order->currency->symbol_left) && !is_null($order->currency->symbol_left) &&
                     $order->currency->symbol_left
                     !=
                     ""){{$order->currency->symbol_left}}@else $@endif{{setDefaultPriceFormat($total->value)}}</td>
                  @else
                  <td colspan="4" class="fw-bolder fs-6 text-gray-800 text-end">{{$total->title}}</td>
                  <td class="fw-bolder fs-6 text-gray-800 text-end">@if (isset($order->currency->symbol_left) && !is_null($order->currency->symbol_left) &&
                     $order->currency->symbol_left
                     !=
                     ""){{$order->currency->symbol_left}}@else $@endif{{setDefaultPriceFormat($total->value)}}</td>
                  @endif
               </tr>
               @endforeach
            </tbody>
         </table>
      </div>
   </div>
</div>

<div class="row">
   <div class="mb-5 col-md-6">
      <label class="form-label" for="r-amount">Remaining Amount </label>
      @php
      $total_amount = setDefaultPriceFormat($order->total);
      /**
      * check to see whether there was a payment entry on `payments` table
      * if there was a `payment` then the `remaining amount` is the amount `payable`
      * if there was no `payment` then the `order total` is the amount `payable`
      */
      list($payment_exists, $remaining_amount) = getRemainingAmountFromPayments($order->id);
      $remaining_amount = ($payment_exists) ? setDefaultPriceFormat($remaining_amount) : $total_amount;
      @endphp
      <input class="form-control form-control-solid" type="text" id="r-amount" value="{{$remaining_amount}}" readonly>
   </div>
</div>