<div class="modal fade" tabindex="-1" id="route-summary-modal">
   <div class="modal-dialog modal-xl">
      <div class="modal-content">
         <div class="modal-header">
            <h2 class="modal-title">Route Summary</h2>

            <!--begin::Close-->
            <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
               <span class="svg-icon svg-icon-2x">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                     <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                        <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1"></rect>
                        <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1">
                        </rect>
                     </g>
                  </svg>
               </span>
            </div>
            <!--end::Close-->
         </div>

         <div class="modal-body">
            <div class="row">
               <div class="col-md-12">
                  <!--begin::Table-->
                  <table id="generic-datatable" class="table table-striped table-row-bordered table-sm gy-2 gs-2 align-middle">
                     <thead>
                        <tr class="fw-bolder fs-6 text-gray-800">
                           <th style="width: 8%;">Order #</th>
                           <th style="width: 15%;">Customer Name</th>
                           <th style="width: 26%;">Address</th>
                           <th class="text-center" style="width: 10%;">Order Status</th>
                           <th class="text-end" style="width: 13%;">Order Total</th>
                           <th class="text-end" style="width: 13%;">Paid Amount</th>
                           <th class="text-end" style="width: 15%;">Remaining Amount</th>
                        </tr>
                     </thead>
                     <tbody>
                        @php
                        $grand_total = 0.0;
                        $grand_paid_total = 0.0;
                        $grand_remaining_total = 0.0;
                        @endphp
                        @if (count($route->route_locations) > 0)
                        @foreach ($route->route_locations as $location)
                        @php
                        $grand_total += $total_amount = setDefaultPriceFormat($location->order->total);
                        /**
                        * check to see whether there was a payment entry on `payments` table
                        * if there was a `payment` then the `remaining amount` is the amount `payable`
                        * if there was no `payment` then the `order total` is the amount `payable`
                        */
                        list($payment_exists, $remaining_amount) = getRemainingAmountFromPayments($location->order->id);
                        $grand_remaining_total += $remaining_amount = ($payment_exists) ? setDefaultPriceFormat($remaining_amount) : $total_amount;
                        $grand_paid_total += ($total_amount - $remaining_amount)
                        @endphp
                        <tr>
                           <td>{{$location->order_id}}</td>
                           <td>
                              {{$location->order->first_name ." ". $location->order->last_name}}
                           </td>
                           <td>
                              {{$location->order->shipping_address_1}}
                           </td>
                           <td class="text-center">{{$location->order->order_status->name}}</td>
                           <td class="text-end">${{$total_amount}}</td>
                           <td class="text-end">${{setDefaultPriceFormat($total_amount - $remaining_amount)}}</td>
                           <td class="text-end">${{$remaining_amount}}</td>
                        </tr>
                        @endforeach

                        @else
                        <tr>
                           <td colspan="7" class="text-center"><strong>No Data Found...</strong></td>
                        </tr>
                        @endif
                     </tbody>
                     <tfoot>
                        <tr>
                           <td colspan="6" class="text-end fw-bolder fs-6 text-gray-800">Total Orders Amount</td>
                           <td class="text-end fw-bolder fs-6 text-gray-800">${{setDefaultPriceFormat($grand_total)}}</td>
                        </tr>
                        <tr>
                           <td colspan="6" class="text-end fw-bolder fs-6 text-gray-800">Total Paid Amount</td>
                           <td class="text-end fw-bolder fs-6 text-gray-800">${{setDefaultPriceFormat($grand_paid_total)}}</td>
                        </tr>
                        <tr>
                           <td colspan="6" class="text-end fw-bolder fs-6 text-gray-800">Total Remaining Amount</td>
                           <td class="text-end fw-bolder fs-6 text-gray-800">${{setDefaultPriceFormat($grand_remaining_total)}}</td>
                        </tr>
                     </tfoot>
                  </table>
                  <!--end::Table-->
               </div>
            </div>
         </div>
      </div>
   </div>
</div>