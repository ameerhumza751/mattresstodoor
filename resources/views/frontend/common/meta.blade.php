<title>{{$meta_title}}</title>
<meta name="keywords" content="{{$meta_keyword}}">
<meta name="description" content="{{$meta_description}}">
<meta name="author" content="{{$meta_title}}">

<meta property="og:title" content="{{$meta_title}}">
<meta property="og:description" content="{{$meta_description}}">
<meta property="og:image" content="{{$meta_image}}">
<meta property="og:url" content="{{$meta_url}}">
<meta name="twitter:card" content="summary_large_image">