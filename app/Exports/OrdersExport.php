<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\View\View;
use App\Models\Admin\Order;
use App\Models\Admin\Option;

class OrdersExport implements FromView, ShouldAutoSize
{
    public $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function view(): View
    {
        ### PARAMS ###
        $date_range = (isset($this->request->date_range) && !is_null($this->request->date_range)) ? $this->request->date_range : "-1";
        $delivery_date_range = (isset($this->request->delivery_date_range) && !is_null($this->request->delivery_date_range)) ? $this->request->delivery_date_range : "-1";
        $order_status_id = (isset($this->request->order_status) && !is_null($this->request->order_status)) ? $this->request->order_status : "-1";
        $customer_id = (isset($this->request->customer_id) && !is_null($this->request->customer_id)) ? $this->request->customer_id : "-1";

        if ($date_range != '-1') {
            $split_date = explode(' to ', $date_range);
        }
        if ($delivery_date_range != '-1') {
            $split_delivery_date = explode(' to ', $delivery_date_range);
        }
        ### INIT QUERY ###
        $query = Order::with([
            'order_status' => function ($q) {
                $q->select('id', 'name');
            },
            'order_products' => function ($q) {
            },
            'order_products.product.discount' => function ($q) {
            },
            'order_products.order_options' => function ($q) {
            },
            'order_options.product_option_value' => function ($q) {
            },
        ])
            ->where('is_deleted', getConstant('IS_NOT_DELETED'));
        ### FETCH ORDER CREATED BY LOGGED IN USER ###
        if (
            !(Auth::guard('web')->user()->hasRole("Super Admin")) &&
            !(Auth::guard('web')->user()->hasRole("Office Admin"))
        ) {
            $query->where('created_by', Auth::guard('web')->user()->id);
        }
        ### ORDER DATE RANGE FILTER ###
        if ($date_range != '-1') {
            $query->whereRaw('DATE(created_at) BETWEEN "' . $split_date[0] . '" AND "' . $split_date[1] . '" ');
        }
        ### CUSTOMER FILTER ###
        if ($customer_id != '-1') {
            $query->where('customer_id', $customer_id);
        }
        ### ORDER DELIVERY DATE RANGE FILTER ###
        if ($delivery_date_range != '-1') {
            $query->whereRaw('DATE(delivery_date) BETWEEN "' . $split_delivery_date[0] . '" AND "' . $split_delivery_date[1] . '" ');
        }
        ### ORDER STATUS FILTER ###
        if ($order_status_id != '-1') {
            $query->where('order_status_id', $order_status_id);
        }
        ### RESULT ###
        $orders = $query->orderBy('id', 'DESC')
            ->get();

        $options = Option::select('id', 'type', 'sort_order', 'status', 'is_deleted')->with([
            'eng_description' => function ($q) {
                $q->select('option_id', 'language_id', 'name');
            }
        ])->where('is_deleted', getConstant('IS_NOT_DELETED'))
            ->get();

        return view('admin.orders.export_excel', compact('orders', 'options'));
    }
}
