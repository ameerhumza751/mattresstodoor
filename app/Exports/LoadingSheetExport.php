<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\View\View;
use App\Models\Admin\Order;
use App\Models\Admin\Option;
use App\Models\Admin\LoadingSheet;

class LoadingSheetExport implements FromView, ShouldAutoSize
{
    public $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function view(): View
    {
        // ### PARAMS ###
        // $delivery_date_range = (isset($this->request->delivery_date_range) && !is_null($this->request->delivery_date_range)) ? $this->request->delivery_date_range : "-1";

        // if ($delivery_date_range != '-1') {
        //     $split_delivery_date = explode(' to ', $delivery_date_range);
        // }
        // ### INIT QUERY ###
        // $query = Order::with([
        //     'order_status' => function ($q) {
        //         $q->select('id', 'name');
        //     },
        //     'order_products' => function ($q) {
        //     },
        //     'order_products.product.discount' => function ($q) {
        //     },
        //     'order_products.order_options' => function ($q) {
        //     },
        //     'order_options.product_option_value' => function ($q) {
        //     },
        // ])
        //     ->where('is_deleted', getConstant('IS_NOT_DELETED'));
        // ### ORDER DELIVERY DATE RANGE FILTER ###
        // if ($delivery_date_range != '-1') {
        //     $query->whereRaw('DATE(delivery_date) BETWEEN "' . $split_delivery_date[0] . '" AND "' . $split_delivery_date[1] . '" ');
        // }
        // ### RESULT ###
        // $orders = $query->orderBy('id', 'DESC')
        //     ->get();

        // $options = Option::select('id', 'type', 'sort_order', 'status', 'is_deleted')->with([
        //     'eng_description' => function ($q) {
        //         $q->select('option_id', 'language_id', 'name');
        //     }
        // ])->where('is_deleted', getConstant('IS_NOT_DELETED'))
        //     ->get();

        // return view('admin.orders.export_loading_sheet', compact('orders', 'options'));

        $options = Option::select('id', 'type', 'sort_order', 'status', 'is_deleted')->with([
            'eng_description' => function ($q) {
                $q->select('option_id', 'language_id', 'name');
            }
        ])->where('is_deleted', getConstant('IS_NOT_DELETED'))
            ->get();

        // return count($options);
        $loading_sheet = LoadingSheet::with([
            'loading_sheet_items' => function ($q) {
                $q->with([
                    'loading_sheet_item_options',
                ]);
            },
        ])->where('id', $this->request->loading_sheet_id)->first();

        /**
         * Preventing `order` loading sheet items duplication
         */
        $items = [];
        if (count($loading_sheet->loading_sheet_items) > 0) {
            foreach ($loading_sheet->loading_sheet_items as $loading_sheet_item) {
                $structured = true;
                /**
                 * generating product name
                 */
                $product_name = $loading_sheet_item->name;

                if (count($loading_sheet_item->loading_sheet_item_options) > 0) {
                    foreach ($loading_sheet_item->loading_sheet_item_options as $loading_sheet_item_option) {
                        $product_name .= "<br> - " . $loading_sheet_item_option->name . ": " . $loading_sheet_item_option->value;
                    }
                }

                // if ($loading_sheet_item->inventory_from == "order") {
                //     $structured = false;
                /**
                 * checking whether the product name already exists in multi-dimensional array
                 */
                if (array_search($product_name, array_column($items, 'name')) !== FALSE) {
                    /**
                     * check whether the index return is a loading sheet item of type `order`
                     */
                    // if ($items[array_search($product_name, array_column($items, 'name'))]['inventory_from'] == "order") {
                    $structured = false;
                    /**
                     * updating loading sheet item quantity
                     */
                    $items[array_search($product_name, array_column($items, 'name'))]['quantity'] += $loading_sheet_item->quantity;
                    // } else {
                    //     $structured = true;
                    // }
                } else {
                    $structured = true;
                }
                // } else {
                //     $structured = true;
                // }

                /**
                 * generating structured result set
                 */
                if ($structured) {
                    $temp['id'] = $loading_sheet_item->id;
                    $temp['loading_sheet_id'] = $loading_sheet_item->loading_sheet_id;
                    $temp['order_id'] = $loading_sheet_item->order_id;
                    $temp['inventory_from'] = $loading_sheet_item->inventory_from;
                    $temp['name'] = $product_name;
                    $temp['price'] = $loading_sheet_item->price;
                    $temp['quantity'] = $loading_sheet_item->quantity;
                    $temp['loading_sheet_item_options'] = $loading_sheet_item->loading_sheet_item_options;
                    $temp['unformatted_name'] = $loading_sheet_item->name;

                    $items[] = $temp;
                }
            }
        }

        return view('admin.loading_sheets.excel_template', compact('loading_sheet', 'options', 'items'));
    }
}
