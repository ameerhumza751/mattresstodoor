<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Admin\Route;
use App\Models\Admin\Order;

class RouteLocation extends Model
{
    use HasFactory;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function route()
    {
        return $this->belongsTo(Route::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    function _insert($route_id, $order_id, $sort_order = '1')
    {
        self::create([
            'route_id' => $route_id,
            'order_id' => $order_id,
            'sort_order' => $sort_order,
        ]);
    }

    function _checkValidityOfOrderList($orders_in)
    {
        return self::with([
            'route' => function ($q) {
                $q->select('id', 'name');
            }
        ])->whereIn('order_id', $orders_in)->get();
    }

    function _destroy($id)
    {
        return self::where('id', $id)->delete();
    }
}
