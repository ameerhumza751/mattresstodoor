<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    function _insert($order_id, $method, $type = null, $mode = null, $paid_amount = 0.00, $remaining_amount = 0.00)
    {
        $payment = self::create([
            'order_id' => $order_id,
            'method' => $method,
            'type' => $type,
            'mode' => $mode,
            'paid_amount' => $paid_amount,
            'remaining_amount' => $remaining_amount,
        ]);

        return $payment;
    }
}
