<?php

namespace App\Models\Admin;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Admin\User;
use App\Models\Admin\Store;
use App\Models\Admin\RouteLocation;
use App\Models\Admin\OrderRouteLog;
use App\Models\Admin\Order;
use App\Models\Admin\LoadingSheet;

class Route extends Model
{
    use HasFactory;

    private function setRouteEndLocation($location, $orders)
    {
        if (is_null($location)) {
            if (!is_null($orders)) {
                $last_order = $orders[key(array_slice($orders, -1, 1, true))];
                $location = $last_order['shipping_address_1'];
            }
        }
        return $location;
    }

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function route_created_by()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function route_assigned_to()
    {
        return $this->belongsTo(User::class, 'assigned_to', 'id');
    }

    public function route_locations()
    {
        return $this->hasMany(RouteLocation::class);
    }

    public function start_location()
    {
        return $this->belongsTo(Store::class, 'start_location_id', 'id');
    }

    public function loading_sheet()
    {
        return $this->hasOne(LoadingSheet::class);
    }

    function _show($id)
    {
        return self::with([
            'start_location' => function ($q) {
                $q->select('id', 'name', 'address', 'lat', 'lng');
            },
            'route_created_by' => function ($q) {
                $q->select('id', 'first_name', 'last_name');
            },
            'route_assigned_to' => function ($q) {
                $q->select('id', 'first_name', 'last_name');
            },
            'route_locations' => function ($q) {
                $q->select('id', 'route_id', 'order_id', 'sort_order')->with([
                    'order' => function ($q) {
                        $q->select('id', 'first_name', 'last_name', 'email', 'telephone', 'currency_id', 'shipping_address_1', 'shipping_lat', 'shipping_lng', 'shipping_city', 'shipping_postcode', 'shipping_country', 'shipping_zone', 'payment_method_code', 'payment_type', 'order_status_id', 'paid_amount', 'remaining_amount', 'total')->with([
                            'order_status' => function ($q) {
                                $q->select('id', 'name');
                            },
                            'currency' => function ($q) {
                                $q->select('id', 'title', 'symbol_left', 'symbol_right');
                            }
                        ])
                            ->where('is_deleted', getConstant('IS_NOT_DELETED'));
                    }
                ])->orderBy('sort_order');
            }
        ])
            ->where('id', $id)
            ->first();
    }

    function _store($request)
    {
        $end_location = (isset($request->end_location) && !is_null($request->end_location)) ? $request->end_location : null;
        $orders = (isset($request->orders) && !is_null($request->orders)) ? $request->orders : null;

        $route = new Route();
        $route->name = capAll($request->name);
        $route->start_location_id = $request->start_location_id;
        $route->end_location = $this->setRouteEndLocation($end_location, $orders);
        $route->created_by = Auth::guard('web')->user()->id;
        $route->assigned_to = 0;
        $route->save();

        $route_id = $route->id;

        if ($request->has('orders')) {
            foreach ($request->orders as $k => $v) {
                (new RouteLocation())->_insert($route_id, $v['id'], $v['sort_order']);
                (new OrderRouteLog())->_insert($v['id'], $route_id);
            }
        }

        return $route;
    }

    function _update($request, $id)
    {
        $end_location = (isset($request->end_location) && !is_null($request->end_location)) ? $request->end_location : null;
        $orders = (isset($request->orders) && !is_null($request->orders)) ? $request->orders : null;

        self::where('id', $id)->update([
            "name" => capAll($request->name),
            "start_location_id" => $request->start_location_id,
            "end_location" => $this->setRouteEndLocation($end_location, $orders),
        ]);

        // delete existing associated route locations and insert new
        RouteLocation::where('route_id', $id)->delete();
        OrderRouteLog::where('route_id', $id)->delete();
        if ($request->has('orders')) {
            foreach ($request->orders as $k => $v) {
                (new RouteLocation())->_insert($id, $v['id'], $v['sort_order']);
                (new OrderRouteLog())->_insert($v['id'], $id);
            }
        }

        return $id;
    }

    function _destroy($id)
    {
        RouteLocation::where('route_id', $id)->delete();
        return self::where('id', $id)->delete();
        // return self::where('id', $id)->update(['is_deleted' => getConstant('IS_DELETED')]);
    }

    function _updateStatus($request, $id)
    {
        $current_status = $request->input('current_status');

        if ($current_status == getConstant('IS_STATUS_ACTIVE')) {
            $new_status = getConstant('IS_NOT_STATUS_ACTIVE');
        } else {
            $new_status = getConstant('IS_STATUS_ACTIVE');
        }

        $update = self::where(['id' => $id])->update(['status' => $new_status]);

        if ($update) {
            $return = array(['status' => true, 'current_status' => $new_status]);
            $res = json_encode($return);
        } else {
            $return = array(['status' => false, 'current_status' => $new_status]);
            $res = json_encode($return);
        }
        return $res;
    }

    function _dataTable($request)
    {
    }

    function _bulkDelete($request)
    {
        // return $request;
        $res = ['status' => true, 'message' => 'Success'];
        $deleted = self::whereIn('id', $request->ids)->update(['is_deleted' => getConstant('IS_DELETED')]);
        if (!$deleted) {
            $res['status'] = false;
            $res['message'] = "Error";
        }
        return $res;
    }

    function _detail($request, $id)
    {
        return $this->_show($id);
    }

    function _assignUnassignOrder($request)
    {
        // return $request;
        $delivery_rep_id = $request->delivery_rep_id;
        $route_id = $request->route_id;
        $res = ['status' => false, 'data' => 'Unable to perform the action.'];

        $update = self::where('id', $route_id)->update(['assigned_to' => $delivery_rep_id]);
        if ($update) {
            $res['status'] = true;
            $res['data'] = "Successfully updated route.";
        }

        return json_encode($res);
    }

    function _checkOrdersRoutes($request)
    {
        return Order::select('id', 'first_name', 'last_name', 'shipping_address_1', 'order_status_id', 'total', 'delivery_date')->with([
            'order_status' => function ($q) {
                $q->select('id', 'name');
            },
            'route_location' => function ($q) {
                $q->select('id', 'route_id', 'order_id')->with([
                    'route' => function ($q) {
                        $q->select('id', 'name');
                    }
                ]);
            }
        ])
            ->where('is_deleted', getConstant('IS_NOT_DELETED'))
            ->whereIn('id', $request->order_ids)
            ->get();
    }

    function _getOptimizedRoutes($id)
    {
        $route = $this->_show($id);
        /**
         * generating locations array required RouteXL tour api.
         */
        $locations = [
            [
                'address' => $route->start_location->address,
                'lat' => $route->start_location->lat,
                'lng' => $route->start_location->lng,
            ]
        ];
        foreach ($route->route_locations as $location) {
            array_push($locations, [
                'address' => $location->order->shipping_address_1,
                'lat' => $location->order->shipping_lat,
                'lng' => $location->order->shipping_lng,
            ]);
        }
        // return $locations;

        /**
         * RouteXL tour api
         * properly formatting RouteXL api response.
         */
        $res = json_decode(initRouteXL($locations));
        Session::forget('optimized_routes');
        session()->put('optimized_routes', $res);
        return $res;
    }

    function _optimizeRoutes($id)
    {
        $original_routes = $this->_show($id)->route_locations->toArray();
        $optimized_routes = session()->get('optimized_routes');

        /**
         * looping over session stored optimized routes for optimizing delivery route.
         */
        foreach ($optimized_routes->data->route as $key => $route) {
            /**
             * ignoring starting location i.e. store location.
             */
            if ($key == 0) {
                continue;
            }
            /**
             * looping over original routes
             */
            foreach ($original_routes as $k2 => $original_route) {
                /**
                 * map optimized route address from $optimized_routes with order address from $original_routes
                 * where `true` update route_location sort_order based on $optimized_routes loop key
                 * after updating sort_order remove that specific order form $original_route and break the loop
                 * this will prevent unwanted sort_order's if multiple same locations exist.
                 */
                if ($route->name == $original_route['order']['shipping_address_1']) {
                    RouteLocation::where('route_id', $id)->where('order_id', $original_route['order']['id'])->update(['sort_order' => $key, 'distance' => $route->distance]);
                    unset($original_routes[$k2]);
                    break;
                }
            }
        }

        Session::forget('optimized_routes');
        return json_encode(['status' => true, 'data' => '']);
    }
}
