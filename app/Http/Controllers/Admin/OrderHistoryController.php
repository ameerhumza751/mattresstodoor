<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Models\Admin\RouteLocation;
use App\Models\Admin\Payment;
use App\Models\Admin\OrderHistory;
use App\Models\Admin\OrderBill;
use App\Models\Admin\Order;
use App\Mail\NotifyOrderStatusChange;
use App\Http\Controllers\Controller;

class OrderHistoryController extends Controller
{
    public function store(Request $request)
    {
        // return $request;
        $request->validate([
            'order_id' => 'required',
            'order_status_id' => 'required',
        ]);

        ### updated order status ###
        $update = ['order_status_id' => $request->order_status_id];
        ### updated delivery date ###
        if (isset($request->delivery_date) && !is_null($request->delivery_date)) {
            $update['delivery_date'] = $request->delivery_date;
        }
        ### updated order ###
        Order::where('id', $request->order_id)->update($update);

        /**
         * Automated order removal from route
         */
        if ($request->order_status_id == "17") { // $request->order_status_id == "17" == "Postpone"
            $is_removable = (isset($request->is_removable) && !is_null($request->is_removable) && $request->is_removable == "1") ? true : false;
            if ($is_removable) {
                RouteLocation::where('route_id', $request->route_id)->where('order_id', $request->order_id)->delete();
            }
        }

        /**
         * get order
         */
        $order = Order::where('id', $request->order_id)->first();

        $is_payment = false;
        if (isset($request->payment_method) && !is_null($request->payment_method) && ($request->payment_method == "COD" || $request->payment_method == "COC" || $request->payment_method == "p-link")) {
            /**
             * check to see whether there was a payment entry on `payments` table
             * if there was a `payment` then the `remaining amount` is the amount `payable`
             * if there was no `payment` then the `order total` is the amount `payable`
             */
            list($payment_exists, $remaining_amount) = getRemainingAmountFromPayments($request->order_id);
            $paid_amount = ($payment_exists) ? $remaining_amount : $order->total;

            $payment_received = (isset($request->payment_received) && !is_null($request->payment_received)) ? ($request->payment_received == "true" ? true : false) : true;

            if (isset($request->payment_mode) && !is_null($request->payment_mode) &&  $request->payment_mode == "cash") {
                (new OrderBill())->_insert($request->order_id, $request->bills, 'remaining');
                $paid_amount = 0;
                $notes = [
                    "hundred" => 100,
                    "fifty" => 50,
                    "twenty" => 20,
                    "ten" => 10,
                    "five" => 5,
                    "two" => 2,
                    "one" => 1,
                ];
                foreach ($request->bills as $key => $value) {
                    $paid_amount += $notes[$key] * $value;
                }
            }

            /**
             * used to make a separate record of payments for accounting 
             * $order_id = `id`
             * $request->payment_method = `payment_method` = `Payment on Delivery`, `Payment on Counter`, `Authorize.net`
             * $request->payment_type = `payment_type` = `full`, `partial`
             * $request->payment_mode = `payment_mode` = `online transfer`, `cash`, `card`
             * $paid_amount = `paid_amount`
             * $remaining_amount = `remaining_amount`
             */
            if (!$payment_received) {
                $is_payment = true;
                (new Payment())->_insert($request->order_id, $order->payment_method, $order->payment_type, $request->payment_mode, $paid_amount, 0.00);
            }
        }

        ### CREATE ORDER HISTORY ###
        $inserted = (!$is_payment) ? (new OrderHistory())->_store($request) : true;

        if ($inserted) {
            if (isset($request->notify) && !is_null($request->notify) && $request->notify == '1') {
                Mail::to($order->email)->send(new NotifyOrderStatusChange($order->id, $order->order_status->name, $order->created_at));
            }
            return redirect()->back()->with('success', 'Order updated successfully.');
        }
    }
}
