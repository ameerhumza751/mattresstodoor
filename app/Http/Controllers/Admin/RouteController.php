<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Admin\User;
use App\Models\Admin\Store;
use App\Models\Admin\Route;
use App\Models\Admin\OrderStatus;
use App\Models\Admin\Order;
use App\Http\Controllers\Controller;

class RouteController extends Controller
{
    public function index()
    {
        ### CONST ###
        $menu_1 = 'sales';
        $active = 'routes';
        $title = 'Routes';

        $query = Route::where('is_deleted', getConstant('IS_NOT_DELETED'))->with([
            'route_created_by' => function ($q) {
                $q->select('id', 'first_name', 'last_name');
            },
            'loading_sheet' => function ($q) {
                $q->select('id', 'route_id', 'name');
            },
        ]);
        /**
         * check whether the authenticated user has role `Delivery Rep` then apply the where condition
         */
        if (Auth::guard('web')->user()->hasRole("Delivery Rep")) {
            $query->where('assigned_to', Auth::guard('web')->user()->id);
        }
        $routes = $query->orderBy('id', 'DESC')
            ->paginate(10);

        $stores = Store::where('is_deleted', getConstant('IS_NOT_DELETED'))
            ->where('status', getConstant('IS_STATUS_ACTIVE'))
            ->get();

        return view('admin.routes.index', compact('menu_1', 'active', 'title', 'routes', 'stores'));
    }

    public function create()
    {
        ### CONST ###
        $menu_1 = 'sales';
        $active = 'routes';
        $title = 'Create Route';
        $type = 'create';

        return view('admin.routes.form', compact('menu_1', 'active', 'title', 'type'));
    }

    public function store(Request $request)
    {
        // return $request;
        $request->validate([
            'name' => 'required',
            'start_location_id' => 'required',
        ]);

        $res = (new Route())->_store($request);
        if ($res) {
            return redirect()->route('routes.index')->with('success', 'Route created successfully.');
        }
    }

    public function show($id)
    {
        return (new Route())->_show($id);
    }

    public function edit($id)
    {
        ### CONST ###
        $menu_1 = 'sales';
        $active = 'routes';
        $title = 'Edit Route';
        $type = 'edit';

        $modal = (new RouteController())->show($id);

        $stores = Store::where('is_deleted', getConstant('IS_NOT_DELETED'))
            ->get();

        return view('admin.routes.form', compact('menu_1', 'active', 'title', 'type', 'id', 'modal', 'stores'));
    }

    public function update(Request $request, $id)
    {
        // return $request;
        $request->validate([
            'name' => 'required',
            'start_location_id' => 'required',
        ]);

        $res = (new Route())->_update($request, $id);
        if ($res) {
            return redirect()->route('routes.index')->with('success', 'Route updated successfully.');
        }
    }

    public function destroy($id)
    {
        $res = ['status' => true, 'data' => 'Successfully deleted Route.'];
        $del = (new Route())->_destroy($id);

        if (!$del) {
            $res["status"] = false;
            $res["data"] = "Error.";
        }
        return json_encode($res);
    }

    public function updateStatus(Request $request, $id)
    {
        return (new Route())->_updateStatus($request, $id);
    }

    public function dataTable(Request $request)
    {
        return (new Route())->_dataTable($request);
    }

    public function bulkDelete(Request $request)
    {
        return (new Route())->_bulkDelete($request);
    }

    public function detail(Request $request, $id)
    {
        ### CONST ###
        $menu_1 = 'sales';
        $active = 'routes';
        $title = 'Route Detail';

        $route = (new Route())->_detail($request, $id);
        $order_statuses = OrderStatus::where('is_deleted', getConstant('IS_NOT_DELETED'))
            ->get();
        ### FETCH DISPATCH MANAGERS ###
        $delivery_reps = [];
        if (
            Auth::guard('web')->user()->hasRole("Super Admin") ||
            Auth::guard('web')->user()->hasRole("Office Admin") ||
            Auth::guard('web')->user()->hasRole("Dispatch Manager")
        ) {
            $delivery_reps = User::whereHas("roles", function ($q) {
                $q->where("name", "Delivery Rep");
            })
                ->where('is_deleted', getConstant('IS_NOT_DELETED'))
                ->get();
        }

        return view('admin.routes.detail', compact('menu_1', 'active', 'title', 'route', 'id', 'delivery_reps', 'order_statuses'));
    }

    public function assignDeliveryRep(Request $request)
    {
        $request->validate([
            'route_id' => 'required',
            'delivery_rep_id' => 'required',
        ]);

        Route::where('id', $request->route_id)->update(['assigned_to' => $request->delivery_rep_id]);
        return redirect()->back()->with('success', 'Route updated successfully.');
    }

    public function checkOrdersRoutes(Request $request)
    {
        $orders = (new Route())->_checkOrdersRoutes($request);
        $order_statuses = [];
        $already_assigned_orders_count = 0;
        foreach ($orders as $order) {
            if ($order->route_location) {
                $already_assigned_orders_count++;
            }
            array_push($order_statuses, $order->order_status->name);
        }
        $view = view('admin.routes.orders_with_routes', compact('orders'))->render();
        return json_encode(['status' => true, 'data' => $view, 'order_statuses' => $order_statuses, 'already_assigned_orders_count' => $already_assigned_orders_count]);
    }

    public function getOptimizedRoutes($id)
    {
        $optimized_routes = (new Route())->_getOptimizedRoutes($id);
        $view = view('admin.routes.optimized_routes', compact('optimized_routes'))->render();
        return json_encode(['status' =>  true, 'data' => $view]);
    }

    public function optimizeRoutes($id)
    {
        return (new Route())->_optimizeRoutes($id);
    }

    public function updateOrder($order_id)
    {
        $order = (new Order())->_getOrderDetail($order_id);
        $view = view('admin.routes.order_detail', compact('order'))->render();
        return json_encode(['status' =>  true, 'data' => $view]);
    }

    public function getOrders(Request $request)
    {
        $view = view('admin.routes.orders_modal')->render();
        return json_encode(['status' =>  true, 'data' => $view]);
    }

    public function getRouteSummary($route_id)
    {
        $route = (new Route())->_detail('', $route_id);
        $view = view('admin.routes.route_summary', compact('route'))->render();
        return json_encode(['status' =>  true, 'data' => $view]);
    }
}
