<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Imports\CountryImport;
use App\Models\Admin\Country;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CountryController extends Controller
{
    public function index()
    {
        ### CONST ###
        $menu_1 = 'system';
        $sub_menu = 'localization';
        $active = 'countries';
        $title = 'Countries';

        $countries = Country::where('is_deleted', getConstant('IS_NOT_DELETED'))
            ->orderBy('id', 'DESC')
            ->paginate(10);

        return view('admin.countries.index', compact('menu_1', 'sub_menu', 'active', 'title', 'countries'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Country $country)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
        //
    }

    public function importExcel(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:xlsx,xls,csv,txt',
        ]);
        ### Dumping Excel Data to JSON ###
        // return Excel::toArray(new CountryImport(), request()->file('file'));
        ### Importing Excel to DB ###
        $res = Excel::import(new CountryImport(), request()->file('file'));
        if ($res) {
            return back()->with('success', 'Countries Imported Successfully');
        }
    }

    public function dataTable(Request $request)
    {
        return (new Country())->_dataTable($request);
    }

    public function search(Request $request)
    {
        return (new Country())->_search($request);
    }

    public function bulkDelete(Request $request)
    {
        return (new Country())->_bulkDelete($request);
    }
}
