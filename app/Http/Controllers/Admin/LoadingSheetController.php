<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Admin\Route;
use App\Models\Admin\LoadingSheetItemOption;
use App\Models\Admin\LoadingSheetItem;
use App\Models\Admin\LoadingSheet;
use App\Http\Controllers\Controller;

class LoadingSheetController extends Controller
{
    public function index()
    {
        ### CONST ###
        $menu_1 = 'sales';
        $active = 'loading-sheets';
        $title = 'Loading Sheets';

        $query = LoadingSheet::with([
            'created_by_user' => function ($q) {
                $q->select('id', 'first_name', 'last_name');
            },
        ]);

        /**
         * check whether the authenticated user has role `Super Admin` then apply the where condition
         */
        if (
            !Auth::guard('web')->user()->hasRole("Super Admin") &&
            !Auth::guard('web')->user()->hasRole("Dispatch Manager") &&
            !Auth::guard('web')->user()->hasRole("Office Admin") &&
            !Auth::guard('web')->user()->hasRole("Delivery Manager")
        ) {
            if (Auth::guard('web')->user()->hasRole("Delivery Rep")) {
                $routes_in = Route::where('assigned_to', Auth::guard('web')->user()->id)->pluck('id');
                $query->whereIn('route_id', $routes_in);
            } else {
                $query->where('created_by', Auth::guard('web')->user()->id);
            }
        }

        $loading_sheets = $query->orderBy('id', 'DESC')
            ->paginate(10);

        return view('admin.loading_sheets.index', compact('menu_1', 'active', 'title', 'loading_sheets'));
    }

    public function store(Request $request)
    {
        // return $request;
        $success = ['status' => true, 'data' => 'Success', 'error' =>  generateValidErrorResponse([])];

        $validator = Validator::make($request->all(), [
            'route_id' => 'required',
        ]);

        if ($validator->fails()) {
            $err['status'] = false;
            $err['data'] = pluckErrorMsg($validator->errors()->getMessages());
            $error_res = generateValidErrorResponse($validator->errors()->getMessages());
            $err['error'] = $error_res;
            return sendResponse($err);
        }

        $res = (new LoadingSheet())->_store($request);

        if ($res) {
            $success['loading_sheet_id'] = $res;
            return sendResponse($success);
        }
    }

    public function detail(Request $request, $id)
    {
        ### CONST ###
        $menu_1 = 'sales';
        $active = 'loading-sheets';
        $title = 'Loading Sheet Detail';

        $loading_sheet = (new LoadingSheet())->_detail($request, $id);

        /**
         * Preventing `order` loading sheet items duplication
         */
        $items = [];
        if (count($loading_sheet->loading_sheet_items) > 0) {
            foreach ($loading_sheet->loading_sheet_items as $loading_sheet_item) {
                $structured = true;
                /**
                 * generating product name
                 */
                $product_name = $loading_sheet_item->name;

                if (count($loading_sheet_item->loading_sheet_item_options) > 0) {
                    foreach ($loading_sheet_item->loading_sheet_item_options as $loading_sheet_item_option) {
                        $product_name .= "<br> - " . $loading_sheet_item_option->name . ": " . $loading_sheet_item_option->value;
                    }
                }

                // if ($loading_sheet_item->inventory_from == "order") {
                //     $structured = false;
                /**
                 * checking whether the product name already exists in multi-dimensional array
                 */
                if (array_search($product_name, array_column($items, 'name')) !== FALSE) {
                    /**
                     * check whether the index return is a loading sheet item of type `order`
                     */
                    // if ($items[array_search($product_name, array_column($items, 'name'))]['inventory_from'] == "order") {
                    $structured = false;
                    //     /**
                    //      * updating loading sheet item quantity
                    //      */
                    $items[array_search($product_name, array_column($items, 'name'))]['quantity'] += $loading_sheet_item->quantity;
                    // } else {
                    //     $structured = true;
                    // }
                } else {
                    $structured = true;
                }
                // } else {
                //     $structured = true;
                // }

                /**
                 * generating structured result set
                 */
                if ($structured) {
                    $temp['id'] = $loading_sheet_item->id;
                    $temp['loading_sheet_id'] = $loading_sheet_item->loading_sheet_id;
                    $temp['order_id'] = $loading_sheet_item->order_id;
                    $temp['inventory_from'] = $loading_sheet_item->inventory_from;
                    $temp['name'] = $product_name;
                    $temp['price'] = $loading_sheet_item->price;
                    $temp['quantity'] = $loading_sheet_item->quantity;
                    $temp['loading_sheet_item_options'] = $loading_sheet_item->loading_sheet_item_options;
                    $temp['unformatted_name'] = $loading_sheet_item->name;

                    $items[] = $temp;
                }
            }
        }

        // return $items;

        return view('admin.loading_sheets.detail', compact('menu_1', 'active', 'title', 'id', 'items'));
    }

    public function addItem(Request $request)
    {
        // return $request;
        $success = ['status' => true, 'data' => 'Success', 'error' =>  generateValidErrorResponse([])];

        $validator = Validator::make($request->all(), [
            'product' => 'required',
            'qty' => 'required',
        ]);

        if ($validator->fails()) {
            $res['status'] = false;
            $res['data'] = pluckErrorMsg($validator->errors()->getMessages());
            $res['error'] = generateValidErrorResponse($validator->errors()->getMessages());
        }

        return $server_res = (new LoadingSheet())->_addItem($request);

        if ($server_res['status']) {
            $res['data'] = $server_res['data'];
        } else {
            $res['status'] = false;
            $res['data'] = $server_res['data'];
            $res['error'] = $server_res['error'];
        }

        return sendResponse($res);
    }

    public function destroy(Request $request, $id)
    {
        $type = $request->type;
        $res = ['status' => true, 'data' => 'Successfully deleted Loading Sheet Item.'];

        if ($type == "loading-sheet") {
            $loading_sheet_item_ids = LoadingSheetItem::where('loading_sheet_id', $id)->pluck('id');
            LoadingSheetItemOption::whereIn('loading_sheet_item_id', $loading_sheet_item_ids)->delete();
            LoadingSheetItem::where('loading_sheet_id', $id)->delete();
            $del = LoadingSheet::where('id', $id)->delete();
        } else {
            LoadingSheetItemOption::where('loading_sheet_item_id', $id)->delete();
            $del = LoadingSheetItem::where('id', $id)->delete();
        }

        if (!$del) {
            $res["status"] = false;
            $res["data"] = "Error.";
        }
        return json_encode($res);
    }
}
