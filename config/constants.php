<?php

return [
    'APP_NAME' => 'Mattress To Door',

    'IS_STATUS_ACTIVE' => '1',
    'IS_NOT_STATUS_ACTIVE' => '0',
    'IS_DELETED' => '1',
    'IS_NOT_DELETED' => '0',

    'DATETIME_DB_FORMAT' => 'Y-m-d H:i:s',
    'DATE_DB_FORMAT' => 'Y-m-d',

    'DEFAULT_LANGUAGE_CODE' => 'en',

    'IS_FEATURED' => '1',
    'IS_NOT_FEATURED' => '0',
    'IS_LIKED' => '1',
    'IS_NOT_LIKED' => '0',

    'IS_EDITABLE' => '1',
    'IS_NOT_EDITABLE' => '0',

    'APP_SETTINGS' => 'app_settings',

    'PRODUCTS_WITHOUT_ADMIN_LVL' => 'all',
    'ADMIN_LVL_PRODUCTS' => 'admin',

    'SUBSCRIBED' => '1',
    'UNSUBSCRIBED' => '0',

    'APPROVED' => '1',
    'UNAPPROVED' => '0',

    'AUTHORIZE_ENV' => 'PRODUCTION',
    'PAYBRIGHT_ENV' => 'PRODUCTION',
];
